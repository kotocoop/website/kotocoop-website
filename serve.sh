#!/bin/bash

set -e 

#docker pull registry.gitlab.com/pages/hugo/hugo_extended:latest

#docker run --network host --rm -p 1313:1313 -v $(pwd):/src registry.gitlab.com/pages/hugo/hugo_extended:latest sh -c "hugo server --disableFastRender"

if [ -z "${HUGO_LANG}" ]; then
    export HUGO_LANG=fi
fi

cp config/_default/config-${HUGO_LANG}.yaml config/_default/config.yaml 

cp static/admin/config-local.tmpl static/admin/config.yml
cat static/admin/common.tmpl >> static/admin/config.yml

pgrep -f decap | xargs kill || true
yes | npx decap-server&

hugo serve --disableFastRender --logLevel debug

#docker build -t kotohugo --progress=plain --build-arg="UID=$(id -u)" .
#docker run --name kotohugo --network host --rm -p 1313:1313 -v $(pwd):/src kotohugo

