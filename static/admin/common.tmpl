collections:
  - name: "en-blogs" # Used in routes, e.g., /admin/collections/post
    label: "Blogs in english" # Used in the UI
    folder: "content/english/blog" # The path to the folder where the documents are stored
    path: "{{slug}}/index"
    media_folder: "" # Save images in the post's own folder instead of the static folder
    public_folder: ""
    create: true # Allow users to create new documents in this collection
    fields: # The fields for each document, usually in front matter
      - { label: "Title", name: "title", widget: "string" }
      - { label: "Sub-header", name: "sub_heading", widget: "string", required: false }
      - { label: "Publish Date", name: "date", widget: "datetime" }
      - { label: "Featured Image", required: false, name: "image", widget: "image" }
      - { label: "Tags", name: "tags", widget: "list", summary: "{{fields.tag}}", field: { label: "Tag", name: "tag", widget: "string" }} # https://github.com/decaporg/decap-cms/issues/4646 
      - { label: "Videos", required: false, name: "videos", widget: "list", field: { label: VideoURL, name: videourl, widget: string } }
      - { label: "Images", required: false, name: "images", widget: "list", field: { label: Image, name: image, widget: image } }
      - { label: "Body", name: "body", widget: "markdown", hint: "Make sure to name added image as thumbnail" }
  - name: "en-articles" # Used in routes, e.g., /admin/collections/post
    label: "Articles in english" # Used in the UI
    folder: "content/english/articles" # The path to the folder where the documents are stored
    path: "{{slug}}/index"
    media_folder: "" # Save images in the post's own folder instead of the static folder
    public_folder: ""
    create: true # Allow users to create new documents in this collection
    fields: # The fields for each document, usually in front matter
      - { label: "Title", name: "title", widget: "string" }
      - { label: "Sub-header", required: false, name: "sub_heading", widget: "string" }      
      - { label: "Publish Date", name: "date", widget: "datetime" }
      - { label: "Featured Image", required: false, name: "image", widget: "image" }
      - { label: "Aliases", name: "aliases", required: false, widget: "list", field: { label: alias, name: alias, widget: string } }
      - { label: "Tags", name: "tags", widget: "list", summary: "{{fields.tag}}", field: { label: "Tag", name: "tag", widget: "string" }} # https://github.com/decaporg/decap-cms/issues/4646 
      - { label: "Videos", required: false, name: "videos", widget: "list", field: { label: VideoURL, name: videourl, widget: string } }
      - { label: "Images", required: false, name: "images", widget: "list", field: { label: Image, name: image, widget: image } }
      - { label: "Body", name: "body", widget: "markdown", hint: "Make sure to name added image as thumbnail" }
  - name: "fi-blogs" # Used in routes, e.g., /admin/collections/post
    label: "Blogs in finnish" # Used in the UI
    folder: "content/finnish/blog" # The path to the folder where the documents are stored
    path: "{{slug}}/index"
    media_folder: "" # Save images in the post's own folder instead of the static folder
    public_folder: ""
    create: true # Allow users to create new documents in this collection
    fields: # The fields for each document, usually in front matter
      - { label: "Title", name: "title", widget: "string" }
      - { label: "Sub-header", name: "sub_heading", required: false, widget: "string" }      
      - { label: "Publish Date", name: "date", widget: "datetime" }
      - { label: "Featured Image", name: "image", required: false, widget: "image" }
      - { label: "Tags", name: "tags", widget: "list", summary: "{{fields.tag}}", field: { label: "Tag", name: "tag", widget: "string" }} # https://github.com/decaporg/decap-cms/issues/4646 
      - { label: "Videos", name: "videos", required: false, widget: "list", field: { label: VideoURL, name: videourl, widget: string } }
      - { label: "Images", name: "images", required: false, widget: "list", field: { label: Image, name: image, widget: image } }
      - { label: "Body", name: "body", widget: "markdown", hint: "Make sure to name added image as thumbnail" }
  - name: "fi-articles" # Used in routes, e.g., /admin/collections/post
    label: "Articles in finnish" # Used in the UI
    folder: "content/finnish/articles" # The path to the folder where the documents are stored
    path: "{{slug}}/index"
    media_folder: "" # Save images in the post's own folder instead of the static folder
    public_folder: ""
    create: true # Allow users to create new documents in this collection
    fields: # The fields for each document, usually in front matter
      - { label: "Title", name: "title", widget: "string" }
      - { label: "Sub-header", name: "sub_heading", required: false, widget: "string" }      
      - { label: "Publish Date", name: "date", widget: "datetime" }
      - { label: "Featured Image", name: "image", required: false, widget: "image" }
      - { label: "Aliases", name: "aliases", required: false, widget: "list", field: { label: alias, name: alias, widget: string } }
      - { label: "Tags", name: "tags", widget: "list", summary: "{{fields.tag}}", field: { label: "Tag", name: "tag", widget: "string" }} # https://github.com/decaporg/decap-cms/issues/4646 
      - { label: "Videos", name: "videos", required: false, widget: "list", field: { label: VideoURL, name: videourl, widget: string } }
      - { label: "Images", name: "images", required: false, widget: "list", field: { label: Image, name: image, widget: image } }
      - { label: "Body", name: "body", widget: "markdown", hint: "Make sure to name added image as thumbnail" }
  - name: "en-units" # Used in routes, e.g., /admin/collections/post
    label: "Units in english" # Used in the UI
    folder: "content/english/units" # The path to the folder where the documents are stored
    path: "{{slug}}/index"
    media_folder: "" # Save images in the post's own folder instead of the static folder
    public_folder: ""
    create: true # Allow users to create new documents in this collection
    fields: # The fields for each document, usually in front matter
      - { label: "Title", name: "title", widget: "string" }
      - { label: "Sub-header", name: "sub_heading", required: false, widget: "string" }      
      - { label: "Publish Date", name: "date", widget: "datetime" }
      - { label: "Featured Image", name: "image", required: false, widget: "image" }
      - { label: "Aliases", name: "aliases", required: false, widget: "list", field: { label: alias, name: alias, widget: string } }
      - { label: "Tags", name: "tags", widget: "list", summary: "{{fields.tag}}", field: { label: "Tag", name: "tag", widget: "string" }} # https://github.com/decaporg/decap-cms/issues/4646 
      - { label: "Videos", name: "videos", required: false, widget: "list", field: { label: VideoURL, name: videourl, widget: string } }
      - { label: "Images", name: "images", required: false, widget: "list", field: { label: Image, name: image, widget: image } }
      - { label: "Body", name: "body", widget: "markdown", hint: "Make sure to name added image as thumbnail" }
