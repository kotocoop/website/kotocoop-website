---
title: Elokoto osuuskunnan eettinen ohjeisto
date: 2021-08-21 12:30:00 +0000
aliases:
  - /code_of_conduct
  - /coc
  - /ohjeisto
---
* Kunnioita muiden ihmisten fyysistä ja henkistä tilaa. Älä kosketa ketään kysymättä lupaa. Muista, että et voi kysymättä tietää toisen ihmisen rajoja. Hae myös omaa tilaa itsellesi, jos se on tarpeen.
* Kohtele muita niin kuin haluaisit itseäsi kohdeltavan.
* Älä tee oletuksia ulkonäön tai toiminnan perusteella. Ole kunnioittava kaikkia kohtaan. 
* Älä tee oletuksia kenenkään seksuaalisuudesta, sukupuolesta, kansallisuudesta, etnisestä alkuperästä, uskonnosta, arvoista, sosioekonomisesta taustasta, terveydentilasta tai liikuntakyvystä.
* Älä nöyryytä tai nolaa muita. Pidättäydy juoruilemasta ja arvostelemasta toisten ulkomuotoa.
* Älä tuo esiin puheessasi, teoissasi tai käyttäytymisessäsi mitään toisen henkilön ominaisuuksiin tai piirteisiin perustuvia stereotypioita.
* Anna tilaa - varmista, että kaikilla on mahdollisuus osallistua keskusteluun. Kunnioita myös toisten yksityisyyttä ja suhtaudu arkaluontoisiin aiheisiin kunnioittavasti.
* Voit antaa asianmukaista ja rakentavaa palautetta sopimattomasta käytöksestä. Kuuntele kaikki sinulle annettu palaute. Yritä suhtautua avoimesti saamaasi palautteeseen ja ota se huomioon tulevaisuudessa. Pyydä anteeksi, jos olet loukannut jotakuta.
* Jos huomaat jonkun muun käyttäytyvän sopimattomasti tai muutoin toimivan Elokoto Osuuskunnan periaatteiden vastaisesti, puutu tilanteeseen.
