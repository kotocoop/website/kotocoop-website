---
title: Sosiaalinen media ja chatit
date: 2024-12-26T10:30:00.000Z
aliases:
  - /contact
tags: []
images:
  - /img/blog/oursocialmediapages.jpg
related_posts: []
enable_contact_form: true
---
## Vaihtoehtoiset alustat

Vaikka nykyiset sosiaalisen median sivustot voivat olla tärkeitä ihmisten tavoittamiseksi, olisi hyvä käyttää avoimen lähdekoodin vaihtoehtoja. Lähetä meille ehdotuksia siitä, mitä alustoja kannattaa käyttää!

### Scuttlebutt

Loistava alusta  <https://scuttlebutt.nz> 

Ota yhteyttä ja löydä ihmisiä #kotocoop-kanavan kautta. Jos tarvitset apua, ota yhteyttä Jeukkuun (https://room.kotocoop.org/alias/jeukku tai jeukku Telegramissa)

### Matrix Spaces

Hyvä vaihtoehto Discordille ja muille chateille on Matrix. Element.io on tällä hetkellä ilmeisesti paras asiakasohjelma. 

Löydät huoneemme täältä  https://matrix.to/#/#public:matrix.kotocoop.org

## Sosiaalinen media

Facebook: <https://facebook.com/kotocoop>

Peertube/Videos: - <https://tube.kotocoop.org/videos/>

Instagram: <https://www.instagram.com/kotocoop>

Suomenkielinen Instagram: <https://www.instagram.com/osuuskuntaelokoto>

## Löydät meidät näistä chateista

Telegram-kanava ja ryhmäkeskustelu: <https://t.me/joinchat/HJFMZkMm1Iqp5sy_t6IVQw>

Matrix/Element: <https://matrix.to/#/#public:matrix.kotocoop.org>

Discord chat : <https://discord.gg/7eVJFCG>
