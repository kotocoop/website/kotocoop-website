---

title: Auroora - Yksikön Säännöt
bgimage: "titleimage.jpg"
unitname: Auroora
layout: rules
date: 2021-09-27 15:30:00 +0000

---

# Koto – Auroora - Yksikön Säännöt

### Osuuskunta

Elokoto Osuuskunta, kutsumanimeltään Koto, on vuonna 2020 Suomessa perustettu osuuskunta. Sen tarkoituksena on kehittää ekologisesti kestävää ja sosiaalisesti tasa-arvoista niukkuuden jälkeistä taloutta (post-scarcity economy). Elokoto Osuuskunnan puheenjohtaja ja ainoa jäsen tällä hetkellä on Juuso Vilmunen. Osuuskunnan hallituksen varajäsen on Juuso Määttä. Osuuskunnalla tarkoitamme tässä kirjoituksessa juuri Elokoto Osuuskuntaa. Auroora on osuuskunnan säännöissä määritelty yksikkö.

### Auroora

Auroora sijaitsee Suomessa, tarkemmin sanottuna Itä-Suomessa, Pohjois-Karjalan maakunnassa ja Rääkkylän kunnassa. Aurooran osoite on Haapasalmentie 800, 82335 Rasivaara, Rääkkylä. Aurooraan kuuluu noin yksi hehtaari maata, 455 m2 päärakennus ja ulkorakennus. Tontilla on pieni metsä, pieni viljelyalue ja iso piha. Aurooralla tarkoitamme tässä kirjoituksessa tätä kokonaisuutta.

### Hyödykkeet

Hyödykkeillä tarkoitamme tässä kirjoituksessa raaka-aineita, puolivalmisteita, valmiita tuotteita, palveluita, koneita, kalustoa, rakennuksia, viljelysmaita, metsiä ja kaikkea, mikä hyödyttää ihmistä jollain tavalla.

### Käyttöoikeus

#### \~ Kaikilla asukkailla on yhtäläinen käyttöoikeus osuuskunnan omistamiin hyödykkeisiin.

Kukaan ei saa mistään syystä minkäänlaista etuoikeutta käyttää osuuskunnan omistamia hyödykkeitä. Esimerkiksi se, että joku on sijoittanut osuuskuntaan tai Aurooraan enemmän rahaa kuin muut tai tekee enemmän työtunteja osuuskunnan tai Aurooran hyväksi kuin muut ei oikeuta häntä saamaan muihin verrattuna minkäänlaisia etuoikeuksia mihinkään osuuskunnan omistamiin hyödykkeisiin.

#### \~ Asukas voi, oman harkintansa mukaan, antaa sopivaksi katsomaansa henkilökohtaista omaisuutta toisten asukkaiden yhteiskäyttöön.

Tämä on kuitenkin vapaaehtoista eikä tähän tule painostaa ketään. Asukas voi milloin tahansa vetää yhteiskäytöstä pois omistamansa tavaran. Jokainen asukas käyttää omaa harkintaa siinä, mitä omistamiaan tavaroita haluaa jakaa yhteiskäyttöön. Jos asukas päättää antaa jonkin tavaran yhteiseen käyttöön, tällöin jokaisella asukkaalla tulee olla yhtäläinen mahdollisuus käyttää kyseistä tavaraa ilman minkäänlaisia etuoikeuksia, niin kauan kuin tuo tavara on yhteiskäytössä.

### Päätöksenteko

#### \~ Sekä yksikön jäsenillä, että asukkailla on oikeus tehdä itsenäisiä päätöksiä liittyen yksikön toimintaan ja hyödykkeiden käyttöön. Päätös varmistetaan kaikilta niiltä joihin päätös vaikuttaa.

Osuuskunta päättää Aurooran isoista linjoista, kuten remonteista, hyödykkeiden hankinnasta ja pois myymisestä.

Asukkaat päättävät itse, kuinka järjestävät jokapäiväisen elämänsä, talon työt ja vapaa-aikansa. Osuuskunta ei tavanomaisesti käytä päätösvaltaa näissä asioissa.

Osuuskunta tarjoaa kuitenkin jatkuvan tuen asukkaille niin, että asukkaat voivat aina kysyä neuvoa osuuskunnalta kaikissa Aurooraan liittyvissä asioissa.

### Kuka päättää, kuka pääsee asumaan

#### \~ Aurooran asukkaat päättävät itse, kenet ottavat Aurooraan uudeksi asukkaaksi.

Osuuuskunnalla on oikeus erityistapauksessa kieltää asukkaaksi ottaminen sellaiselta ihmiseltä, kenen se ei katso sopivan Aurooraan asukkaaksi. Osuuskunta ei voi kuitenkaan määrätä, että asukkaaksi on otettava juuri jokin tietty ihminen.

Auroorassa asukkaat elävät ja ovat tekemisissä toistensa kanssa enemmän tai vähemmän joka päivä. On siis tärkeää, että he viihtyvät keskenään. Siksi on tärkeää, että asukkaat itse päättävät siitä, kenet he ottavat uudeksi asukkaaksi ja kenet eivät. Kaikkien Aurooran asukkaiden on kannatettava uuden ihmisen mukaan tuloa, että hänet voidaan ottaa asukkaaksi.

### Kuka pääsee asumaan

#### \~ Pysyväksi asukkaaksi Aurooraan pääsee kuka tahansa, joka kokee omakseen ja hyväksyy Elokoto Osuuskunnan ja Aurooran periaatteet, tavoitteet ja säännöt.

Asukaalta edellytetään hyvää ymmärrystä siitä, että hän ei tiedä kaikesta kaikkea ja avoimuutta uusien asioiden edessä. Hänen tulee kunniottaa ja pystyä tekemään yhteistyötä hyvin monenlaisten ihmisten kanssa, myös niiden, jotka ajattelevat ja elävät eri tavalla kuin hän. Hänellä tulee olla hyvät tunnetaidot eli hänen tulee pystyä ilmaisemaan tunteitaan, ajatuksiaan ja tarpeitaan loukkaamatta toisia ihmisiä ja sallimaan myös se, että toiset ihmiset tekevät saman. Hänen omaksi kokemaansa talousideologiaansa kuuluu se, että hän ei halua kilpailla taloudellisesti kenenkään kanssa, vaan haluaa jakaa tasapuolisesti kaikkien kanssa niin työt kuin työn tulokset. Hän ymmärtää, että ihmiset ovat erilaisia ja että kaikille ei sovi sama talousideologia – hän ei halua laittaa kaikkia samaan muottiin. Hänen toimintaa ohjaa vahva autonominen ja sisäinen motivaatio.

Auroossa ei ole sijaa fanaatikoille eikä fyysisesti ja psyykkisesti väkivaltaisille ihmisille, ei manipuloijille eikä kenellekään muullekaan, joka on valmis vahingoittamaan toista ihmistä.

### Koeaika uusille asukkaille

#### \~ Uusille asukkaille järjestetään koeaika. Asukkaat voivat päättää koeajan pituuden.

Osuuskunnan suositus on, että uudeksi asukkaaksi haluavat viettävät Auroorassa ensin kaksi viikkoa, sen jälkeen kaksi kuukautta. Tämän jälkeen vielä vuoden, jonka jälkeen päätös pysyväksi asukkaaksi ottamisesta voidaan tehdä.

### Erottaminen Auroorasta

#### \~ Rikkomukseksi katsotaan toimiminen tahallisesti Elokoto Osuuskunnan tai Aurooran periaatteiden tai sääntöjen vastaisesti

Jokaisesta rikkomuksesta käynnistetään asukkaiden ja osuuskunnan yhteyshenkilöiden kesken menettely asian selvittämiseksi.

#### \~ Osuuskunnan hallituksen päätöksellä asukas voidaan erottaa Auroorasta

toistuvista rikkomuksista tai jos rikkomus on erityisen törkeä, asukas voidaan erottaa Auroorasta.

Erotettu asukas joutuu muuttamaan pois Auroorasta.

### Työ

#### \~ Asukkailla on oikeus, mutta ei velvollisuutta osallistua Aurooran ylläpitotöihin

Auroorassa kaikilla asukkailla on oikeus tehdä kaikkia talon töitä niin sisällä rakennuksissa kuin ulkona pihallakin sikäli, kun hänen taitonsa riittävät tekemään kyseistä työtä. Yhdelläkään Aurooran asukkaalla ei ole velvollisuutta osallistua millään tavalla mihinkään talon töihin. Kenenkään ei tule painostaa eikä pakottaa toista olemaan tekemättä jotain, mitä hän haluaa tehdä eikä tekemään jotain, mitä hän ei halua tehdä.

### Lapset

#### \~ Auroorassa voi asua lapsiperheitä ja vierailla lapsia ja lapsiperheitä.

Kaikki asukkaat sitoutuvat siihen, että Auroora on turvallinen asuinpaikka myös lapsille.

### Eläimet

#### \~ Auroorassa voi asua ja vierailla lemmikkieläimiä. Auroorassa ei kuitenkaan saa pitää tuotantoeläimiä.

Esimerkiksi pelastettujen tuotantoeläinten pitäminen ja niiden hyödyntäminen on kielletty.

### Tuotanto

Auroora voi periaatteessa tuottaa itselleen mitä vain sen asukkaat osaavat ja haluavat tuottaa.

Osuuskunnalla on oikeus käyttää Aurooran tiloja tuotannossa. Tämän tulee tapahtua yhteisymmärryksessä asukkaiden kanssa.

### Eettinen kestävyys

#### \~ Aurooraan ei tule hankkia uusia eläinperäisiä tuotteita tai hyödykkeitä

Jo hankittuja eläinperäisiä tuotteita saa säilyttää ja käyttää Auroorassa.

#### \~ Auroorassa käytettyjen tuotteiden ja hyödykkeiden eettinen kestävyys tulee arvioida

Epäselvissä tilanteissa eettisyys arvioiden yhteistyössä muiden asukkaiden kanssa

#### \~ Luonnossa eläviä tai kuolleita eläimiä ei tule ajatella hyödykkeinä

Esimerkiksi kalastettuja kaloja ei tule käsitellä Auroorassa

### Ekologinen kestävyys

#### \~ Kaikki toiminta Auroorassa tähtää ekologiseen kestävyyteen.

Aivan alussa täydellinen ekologinen kestävyys ei ole mahdollista, mutta kuljemme koko ajan sitä kohti yhdessä ja meistä jokainen parhaan kykymme mukaan.

Jätteet lajitellaan mahdollisuuksien mukaan. Biojäte laitetaan keräysastiaan tai kompostiin.

Meillä on myös sekajateastia jätteelle, jota ei ole mahdollista kierrättää. Tämä jäte päätyy poltettavaksi energiaksi.

### Suhtautuminen toisiin Auroorassa asuviin ihmisiin

* Kunnioita toisten ihmisten fyysistä ja psyykkistä tilaa. Älä koske ketään ilman lupaa. Muista, että et voi tietää toisen ihmisen rajoja ilman kysymistä. Ota tilaa myös itsellesi, jos koet siihen tarvetta.
* Kohtele muita niin kuin haluaisit muiden kohtelevan sinua.
* Älä tee toisesta ihmisestä oletuksia perustuen toisen ulkonäköön tai käyttäytymiseen. Kunnioita jokaista.
* Älä tee oletuksia toisen seksuaalisesta suutautumisesta, sukupuolesta, kansallisuudesta, etnisyydestä, uskonnosta, arvoista, sosio-ekonomisesta taustasta, terveydestä tai työkyvystä.
* Älä nöyryytä äläkä nolaa ketään. Pidättäydy juoruilusta ja toisen ihmisen ulkoisen olemuksen tuomitsemisesta.
* Älä suhtaudu toiseen ihmiseen stereotypioden mukaan, jotka perustuvat hänen persoonallisuuteen tai luonteeseen.
* Anna toisille ihmisille tilaa – pidä huoli, että kaikilla on mahdollisuus osallistua keskusteluun. Kunnioita toisten yksityisyyttä ja käsittele herkkiä ja arkoja puheenaiheita kunnioituksella.
* Voit antaa asiallista ja rakentavaa palautetta epäasiallisesta käyttäytymisestä. Kuuntele kaikki palaute, mitä toiset ihmiset sinulle antaa. Yritä olla avoin saamaasi palautetta kohtaan ja ota se huomioon tulevaisuudessa. Pyydä anteeksi, jos olet loukannut jotakuta.
* Jos huomaat, että joku toinen käyttäytyy sopimattomasti tai millä tahansa tavalla Elokoto-Osuuskunnan ja Aurooran perusperiaatteiden vastaisesti, puutu tilanteeseen.

### Suhtautuminen Aurooran ulkopuolisiin ihmisiin

Sama kuin edellinen kohta.

Aurooran asukkaan haluavat elää rauhassa ja sopusoinnussa Aurooran ulkopuolistenå ihmisten kanssa. Siksi suhtaudumme kunnioituksella kaikkiin ihmisiin myös Aurooran ulkopuolella. Vaikka Aurooran eräs tarkoituksista on muuttaa maailmaa, emme tee sitä lähetyssaarnaamalla, vaan esimerkin kautta.

”Jos emme sa Aurooraa toimimaan, mitkään sanat eivät saa muita vakuuttumaan ajatustemme toimivuudesta. Jos saamme Aurooran toimimaan, sanoja ei tarvita.”

Toki hyvässä hengessä voimme keskustella halukkaiden kanssa ajatuksistamme. Emme kuitenkaan tyrkytä ajatuksiamme kenellekään. Emme myöskään syrji ketään sen takia, että hän ajattelee jostain asiasta eri tavalla kuin me.

### Sääntöjen uusiminen

Aurooran sääntöjä uusimista käsitellään aina silloin, kun yksikin Aurooran asukas tai osuuskunnan hallituksen jäsen tekee aiheesta aloitteen.  Sääntöjen muutos tehdään neuvottelussa osuuskunnan yhteyshenkilön ja Aurooran jäsenten välillä. Ellei yksimielisyyttä löydy, osuuskunnan hallitus päättää muutoksesta.