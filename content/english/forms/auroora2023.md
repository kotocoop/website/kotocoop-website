---
layout: form
title: 'Auroora 2023'
date: 2023-04-01 22:00:00 +0000
tags: [form]
related_posts: []
rssexclude: true
submit_to: https://formspree.io/f/mgebpkvk

formitems:
  - type: text
    title: What is your name?
    id: name
    placeholder: name
  - type: email
    title: "Your email"
    id: email
  - type: textarea
    title: What do you think of the whole idea of giving someone a chance to live in Auroora?
    id: idea
    rows: 5
  - type: textarea
    title: Please tell about your plans for the summer in Auroora
    id: plans
    rows: 5
  - type: textarea
    title: When are you interested in coming and how long would you like to stay?
    id: when
    rows: 2
  - type: textarea
    title: Are you interested in coming by your self or with a group of people?
    id: who
    rows: 2
  - type: textarea
    title: Do you have any questions for us? We'll answer when we get back to you.
    id: questions
    rows: 4
#  - type: checkboxes
#    title: checkboxes jee
#    id: cbs
#    items:
#      - label: testing 
#        value: ou
#      - label: testing2
#        value: nou
---

Please read this post before answering [english](https://kotocoop.org/blog/2023-01-16-stay-in-auroora/) or [suomeksi](https://kotocoop.org/blog/2023-01-17-asumaan-aurooraan/)

Voit vastata myös suomeksi!
