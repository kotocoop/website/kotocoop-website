---

title: Auroora
title_content: North Karelia, Finland
bgimage: "titleimage.jpg"
unitname: auroora
unitpage: main
date: 2021-09-27 15:30:00 +0000
filtertag: "auroora"
blogcount: 10
short: "Our main investment currently is a place called 'Auroora'. It is an old school building located in Rääkkylä, Finland."
images:
 - /img/units/auroora/auroora_outside.jpg

############################# Navigation ##################################
menus:
- main:
  - { name: Co-op, url: "../.." }
  - { name: Auroora, url: "" }
  - { name: Images, url: "images" }  
  - { name: Unit rules, url: rules }
---

# Auroora

## Out first unit!

We purchaced and founded Auroora in the summer of 2021.

Check out [photos of the place](images)

## Location

Auroora located in Rääkkylä, Finland. From Auroora to Rääkkylä central, where grocery store, bank, pharmacy and other services are located, is about 16km trip. 

Rääkkylä is located in eastern part of Finland and to the closest bigger city it is about a 60km drive. Closest train station is about 20km away at Kitee and from Helsinki to Kitee it is a 4.5h trip. 

The location in the middle of country side, lakes and forests offers a lot. In Finland surrounding forests are free to use based on ["everyman's rights"](https://www.nationalparks.fi/everymansright).

## Building

The building is an 455 square meter school building built in the beginning of 20th century. In its current state, the building can host 4-5 people and after renovation and organizing the rooms a little differently, a few more. Two of the rooms are quite big since one was a small school gymnasium and the other was in other common usage. Those rooms will be great places for an office or other workspaces. In the basement there is a workshop suitable for metal work, sauna, cool storage rooms for food and other storage rooms. It will be a great place for communal living!

## Land area

The plot is a bit over one hectare. On the plot there is some forest, apple trees and a large outbuilding that used to be storage, garage and a cowshed.

We have planted 14 more bushes and two trees in the autumn of 2023.

[Here are our yearly plans and tracking](https://kotocoop.org/units/auroora/garden/)

## Joining

We are looking for people who are interested in visiting or living in Auroora. Please fill this [questionnaire](https://kotocoop.org/joining_a_unit), if you are interested.
