---
title: Tasks
date: 2023-01-07 10:00:00 +0000
layout: post
images:
- img/blog/default.png
---

A new [tasks](https://kotocoop.org/tasks) (https://kotocoop.org/tasks) page has been published. 
Over there you can check out what is happening in Koto Co-op and what are the things people are working on. 

It also a request for anyone to join the action, pick up a task you want to work on and start contributing.

We try to design the tasks so that it shouldn't take too many hours to finish one so all the work is divided in a pretty small chunks.
