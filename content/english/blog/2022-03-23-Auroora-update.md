---
layout: post
title: '2022 Auroora spring update'
sub_heading: ''
date: 2022-03-23 10:30:00 +0000
tags: [ "auroora" ]

images:
- /img/units/auroora/auroora_winter.jpg

more_images:
- /img/units/auroora/20220226_151622.jpg
- /img/units/auroora/20220226_150811.jpg

related_posts: []

---

Work to get Auroora up and running for the next summer has been going on during the winter and next steps are being taken.

We have a small outdoors planning team that is looking into layout for the plot, permaculture and other options to design land usage for agriculture, food forest and all the other activity we want to have there; outdoor kitchen, sauna area, construction site, etc.

The piping in the main building have been reviewed and tested out partly. Plans where to install new pipes will be created and overall renovation plan updated. Rebuilding the damaged rooms should start in a two weeks.

https://kotocoop.org/units/auroora/

