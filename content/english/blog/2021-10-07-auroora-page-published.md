---
layout: post
title: 'Auroora page published'
sub_heading: ''
date: 2021-10-10 10:30:00 +0000
tags: [ "auroora" ]

images:
- /img/units/auroora/auroora_outside.jpg

related_posts: []

---

Koto Co-op Auroora unit has now its own section on our website. There is some photos and more will come. We'll probably post more Auroora specific updates there too. Unit rules are still work in progress and will be published when they are ready.
 https://kotocoop.org/units/auroora/

If you are interested in living there, please fill this questionnaire https://kotocoop.org/joining_a_unit and we'll get in contact with you.