---
layout: post
title: 'Koto Co-op 2022 in photos'
sub_heading: ''
date: 2022-12-26 14:30:00 +0000
tags: []
related_posts: []

images:
  - /img/blog/2022-in-photos-cover.jpg
skip_image: true

embed_videos:
  - https://tube.kotocoop.org/videos/embed/f8dd1766-7271-4ea3-9270-b5102892c774

---


