---

title: Learn

image: /img/blog/blog-post-1.jpg

aliases:

- /learn

layout: learn

date: 2022-03-15 15:30:00 +0000

---

## Purpose

The purpose of Koto Co-op is the development of technologies and social structures that enable sustainable, post-scarcity society, thus making this development feasible. To advance this development, we aim at creating operational models which allow us to gather people and provide them livelihood. This way we sustainably strive to maintain development and research.

## Part of a larger community

As a community, we feel like we are part of a larger global movement towards a more sustainable world. Our core principles, which we share with the movement, and that guide our actions are the following: sustainable development, focus on welfare, sense of community, open source, and sharing economy. We aspire to co-operate with parties that share the same principles and goals with us. Through this co-operation, we can work more efficiently to achieve our goals.

## Development of technologies

The core of our development work is based on developing life-sustaining and livelihood enabling technologies. We attempt to take advantage of the efficiency made possible by technology, but we will not begin to develop industry scale solutions — at least not for now. We develop, for example, solutions related to food and energy production, general production like recycling, manufacturing, refinement, and treatment of materials. In addition to the previous examples, the solutions for infrastructure, building, installing, and maintenance are as well included in our development work. In fact, we strive to develop and research all society supporting technologies in a smaller scale. In our work, we utilize free and open-source solutions as much as possible. The results of our development will be openly shared on the Internet.

## Organization

Our model of operation is to organize into different units and teams. Teams work on different subject areas, thus allowing one to participate according to one’s skills and interests. For the most part, development work is done in these teams. Units, on the other hand, mainly function locally and attempt to provide livelihood for their members. Inside these units, technologies developed in the co-op are implemented and utilized. Some of the units might resemble “high-tech eco-villages”, whereas the others are solutions more suitable for city living. These operational models are not fixed, but the main function of the co-op is to develop them to become as functional, sustainable, and efficient as possible. Along with new members, we strive to develop operational models that allow all the interested to participate.

## Finances

Considering the finances of Koto, it will try to grow its self-sufficiency gradually while expanding activity. After all, the co-op will need sources of income to sustain activity and above all, to grow and develop. The main source of income will be work billed through the co-op and selling the products Koto produces. Other possible income sources include, for instance, donations, member fees, crowdfunding or funding applied for projects.

## Positive feedback

Our core goal is to create a virtuous circle, in which we can utilize and implement technologies we have developed, and with the help of those, to spread and improve our actions thus developing even better techniques and technologies.

## Technology and livelihood

With technology, we will attempt to provide high livelihood for the members of the co-op by doing the minimum amount of work as sustainably as possible. As a result of our development, we will reduce the amount of work needed to sustain the finances of Koto, which allows us to work more on development, thus leaving more resources for new people and expanding the co-op.

While we develop new ways of providing livelihood for our members, we will gradually decrease our dependency on outward trade and monetary economy. Even though we do not necessarily strive for full self-sufficiency, with a degree high enough, our activity remains sustainable when we are not dependent on external income; the less we are dependent on it, the less we should support it.

## When we have succeeded

We have succeeded in our goal when Koto can give people a change to use their time to develop life supporting technologies without having to worry about their livelihood. The further we advance this goal, the more opportunities we can offer to people who wish to join just for the livelihood and security provided by technology. These people would be maintaining the implementations without having to take part in the development process.

## The legal structure

A few words about the legal structure of the co-op; all the activity is guided by the rules of the co-op. According to those rules, Koto Co-op is a nonprofit organization. The co-op will not share dividends which means that all the possible surplus will be invested in its own activity. The board, which in the beginning has only one member, acts as the operative management of the co-op. The operation of the co-op is decided by the assembly of members where every member has the right to vote. The activity of the co-op is guided and defined by the Finnish Law.

## We need you

Koto Co-op was founded to support working for a sustainable world. To succeed in our goal, we need you. Activity in the co-op will take shape depending on what kind of skills and interests people interested in joining have. We need people who have passion to work on meaningful matters even if there is not enough time in the day. We will try to organize ways in which every one can work in the favor of the co-op.

## Contact us

If you are interested in joining the action contact us:

* Facebook <https://www.facebook.com/kotocoop/>
* Telegram <https://t.me/kotocoop>
* Email info@kotocoop.org




