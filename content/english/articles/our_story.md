---

title: Story of Koto
date: 2020-11-27 22:00:00 +0000

---

## -2017

Some time after 2015 in TZM/TVP spaces, mostly on Facebook, there was a few discussion threads about how we can actually create the transition to RBE. A facebook group called "transition phase to RBE/Finland" played a big role.

There was a lot of discussion whether we should create an intentional community, a business, NPO or something else, but nothing seemed to be quite right.

From those discussion came the idea of creating a co-op that would create small communities of 5-10 people called units. Those units would gather funding for the next unit and so the speed of growth would be accelerating.

## 2017

"trade-free co-op" document in finnish was written in january 2017

The document detailed ideas of stages of the process, ownership model, what technologies should be used, rules of the co-op and so on. You can read it here <https://kotocoop.org/description/>

(half) Jokingly simple models were created that showed an estimation of 8 billion people living in the units in 25-80 years.

the idea was discussed mostly with TZM current and former activists during the year and we tried to start the project with a few people in autumn.

Not enough interest was found and because of lack of time the idea was put on hold.

A lot of learning happened during the years. Construction, calculations and so on.

## 2018

In the spring of 2018 the description document was translated to English and published.

We were too busy with other activism in TZM, other organizations, work and problems with personal life so the idea was mostly talked when ever people met.

## 2019

In 2019 we had pretty much given up on the idea (and pretty much in TZM too), but Jeukku joined TZM European meeting in Sweden in during the summer and it was such a great event that passion for building the better world was awaken again. After the event Jeukku decided to commit to creating the co-op.

## 2020

After some huge setbacks and reflection of activism, finally in spring 2020 Jeukku and Juuso in TZM Finland decided to make Koto Co-op its main project.

We started setting up the organization and processes to get things done and we had a few projects about energy created with biogas, *what other*, etc and in the summer we finally made a Koto Co-op official business located Finland.

A new model for operations was created which approached the idea from a bit of a different perspective (<https://kotocoop.org/about/model/>)

At first we made funding our main goal since we agreed that acquiring the first unit is our top priority. To get there we need to make Koto a working business that can sell services and accept support from the public.

In the autumn of 2020 we decided that in 2021 spring, we will found our first unit with what ever funding we have at the time.

During the last few months we have been developing the organization, focused on funding and recruiting more people in the teams.

## 2021

The future is ours to build!