---
title: Irrigation system for Auroora
date: 2024-06-09 10:00:00
layout: singletask
aliases:
- /task/irrigationsystemforauroora
---

## Goal of this task

Let's design and build an automated irrigation and rainwater harvesting system.

## About the task

Ask _Juuso Vilmunen (juuso@vilmunen.net_ or just send a message on some platform) for editing rights.

Don’t use too much time on one topic in the first go. Go through all the parts and start over if you have more time. Let's try to keep an agile approach.

# Content

For irrigation in Auroora we should use water from a combination of well, rainwater harvesting and local water supply. There are various ways to implement this and this document explains how and in what steps a system in Auroora will be built.

We also have to take into account that the system (or at least parts of it) has to be removed in the preparation for the winter and rebuilt every spring.

The main building is about two meters higher than the garden so valves between containers are needed, but probably not pumps.

## Rainwater harvesting

### Containers

## Automation

### Central computer

Possibly Home assistant. We should be able to connect to it outside of Auroora over the internet.

### Electrical switches

### Valves

### Pumps

#### Well

Submersive pump is typically used in wells. A cheap one will probably do [https://www.k-rauta.fi/tuote/uppopumppu-opal-400w-q400-b3m/6413820890203](https://www.k-rauta.fi/tuote/uppopumppu-opal-400w-q400-b3m/6413820890203)

Maybe the easiest way to use it is to start it when water level in a container is low.

## Design

SVG file for planning is available. Here is a current design [https://cloud.kotocoop.org/index.php/s/o2cXi4FqbrZQ7jk](https://cloud.kotocoop.org/index.php/s/o2cXi4FqbrZQ7jk)



