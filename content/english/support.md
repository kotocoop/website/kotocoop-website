---
layout: support
title: Support us
date: 2020-08-01 03:00:00 +0000
image: /img/blog/blog-post-1.jpg
---

## Current situation

The current price of a share is 12500€ and two shares has been acquired by a founding member.
 
Our current income is around 580€ in a month from crowd funding and investment.

### Our current property

#### Unit in Rääkkylä
- real estate in [Rääkkylä](/blog/2021-07-16-a-place-for-our-first-unit/)
- a car
- electric bike
- power tools and appliances

#### Other
- Access to power tools, other tools and a workshop in Finland.

## How income is be used?

- Supporting the work of our technology team.
- Monthly expenses of our first unit until finances there are balanced
- other expenses like web hosting and other services

## Options how you can support us

### Open collective

[https://opencollective.com/kotocoop/contribute](https://opencollective.com/kotocoop/contribute)

<script src="https://opencollective.com/kotocoop/contribute/button.js" color="white"></script>
<br/>

